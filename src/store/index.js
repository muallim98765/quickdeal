import { createStore } from "vuex";


const store = createStore({
	state: {
		tasks: [
			{
				picture: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR_nLCu85ayoTKwYw6alnvrockq5QBT2ZWR2g&usqp=CAU',
				title: 'HTML',
				content: 'Изучение стоит начать с HTML, так как это пригодится при дальнейшем изучении CSS и JS. На этом этапе важно изучить HTML-теги и как их семантически правильно использовать. Даже опытные разработчики не всегда задумываются, какой HTML-тег семантически правильно использовать, а это влияет на доступность веб-приложений, так называемое Web-accessibility и SEO, о чём расскажем чуть дальше.',
				loading: false,
				id: 1,
				done: false,
				deleted: false,
			},
			{
				picture: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR_nLCu85ayoTKwYw6alnvrockq5QBT2ZWR2g&usqp=CAU',
				title: 'CSS',
				content: 'В начале мы советуем получить базовые знания о свойствах, селекторах, понятиях специфичности и каскада стилей. Важно разобраться с понятием “вес селектора» и изучить свойства display и position: они является ключевыми при создании сложных макетов. Для добавления адаптивности в вёрстку необходимо познакомиться с media query.',
				loading: false,
				id: 2,
				done: false,
				deleted: false,
			},
			{
				picture: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR_nLCu85ayoTKwYw6alnvrockq5QBT2ZWR2g&usqp=CAU',
				title: 'JS',
				content: 'Изучив HTML и CSS, вы уже сможете создавать WEB-страницы и делать их красивыми. Вы даже можете создавать формы и отправлять запросы на сервер. Для реализации более сложных интерфейсов, SPA-приложений необходимы знания JS. Для начала необходимо изучить базовый синтаксис языка, познакомиться с основными концепциями, научиться работать с HTML-элементами внутри JS. Также необходимо изучить, что из себя представляет асинхронность и как делать базовые запросы с помощью fetch.',
				loading: false,
				id: 3,
				done: false,
				deleted: false,
			},
			{
				picture: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR_nLCu85ayoTKwYw6alnvrockq5QBT2ZWR2g&usqp=CAU',
				title: 'Алгоритмы и структуры данных',
				content: `Оптимизация производительности. Фронтенд-приложения работают в браузере, и эффективные алгоритмы и структуры данных позволяют создавать быстрые и отзывчивые интерфейсы
				Решение сложных задач. Знание алгоритмов и структур данных может быть необходимо для эффективного решения таких задач, как анимации, обработка графики и другие
				Взаимодействие с бэкендом. Понимание работы определенного алгоритма на сервере может помочь оптимизировать взаимодействие с данными на фронтенде`,
				loading: false,
				id: 4,
				done: false,
				deleted: false,
			},
			{
				picture: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR_nLCu85ayoTKwYw6alnvrockq5QBT2ZWR2g&usqp=CAU',
				title: 'Git',
				content: 'Нужно понять, какие проблемы решает эта технология, изучить базовые команды clone, pull, commit, push, merge и тд. ',
				loading: false,
				id: 5,
				done: false,
				deleted: false,
			},
			{
				picture: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR_nLCu85ayoTKwYw6alnvrockq5QBT2ZWR2g&usqp=CAU',
				title: 'Пакетные менеджеры',
				content: 'Перед изучением фреймворка необходимо изучить пакетные менеджеры. Менеджеры позволяют удобно работать с зависимостями вашего приложения. Все они имеют схожие CLI-команды. Поэтому изучив один, разобраться в другом не составит проблем. Отличия — в алгоритмах для хранения зависимостей/установки/скачивания. Npm превосходит по популярность своих конкурентов в десятки раз.',
				loading: false,
				id: 6,
				done: false,
				deleted: false,
			},
			{
				picture: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR_nLCu85ayoTKwYw6alnvrockq5QBT2ZWR2g&usqp=CAU',
				title: 'Фреймворки',
				content: 'Нет плохого фреймворка, если правильно его использовать. У каждого есть плюсы и минусы. Понятие «удобно или не удобно писать» — очень субъективное. Кто-то ругает Angular за сложность входа, но люди, которые пришли во фронтенд из бэкенда, будут рады встроенной архитектуре и Typescript. Материалов про выбор фреймворка очень много. Попробуем выделить, что нужно помнить при выборе.',
				loading: false,
				id: 7,
				done: false,
				deleted: false,
			},
		]
	},
	getters: {
		tasks(state) {
			return state.tasks.filter((task) => !task.deleted)
		},
  },
	mutations: {
		setTask(state, {status, payload}) {
			if (status == 'new') {
				state.tasks.push({...payload, picture: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR_nLCu85ayoTKwYw6alnvrockq5QBT2ZWR2g&usqp=CAU'})
			} else {
				let task = state.tasks.find((task) => task.id == payload.id)
				task = {...payload, picture: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR_nLCu85ayoTKwYw6alnvrockq5QBT2ZWR2g&usqp=CAU'}
			}
		},
		deleteTask(state, {id}){
			let task = state.tasks.find((task) => task.id == id)
			task.deleted = true
		},
		doneTask(state, {id}){
			let task = state.tasks.find((task) => task.id == id)
			task.done = true
		}
	}
});
export default store;