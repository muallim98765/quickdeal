import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/index.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView,
    },
    {
      path: '/form/:id',
      name: 'form',
      component: () => import('@/views/form.vue')
    },
    {
      path: '/tasks',
      name: 'tasks',
      component: () => import('@/views/tasks.vue')
    }
  ]
})

export default router
