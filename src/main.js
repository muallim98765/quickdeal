import './assets/main.css'

import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from "./store";
import Antd from "ant-design-vue";
import * as AntdIconsVue from "@ant-design/icons-vue";

const app = createApp(App)

app.use(router)
app.use(store);
app.use(Antd);
for (const [key, component] of Object.entries(AntdIconsVue)) {
  app.component(key, component);
}

app.mount('#app')
